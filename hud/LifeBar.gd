tool
extends HBoxContainer

export(int) var life_count = 0 setget set_life_count

var LifeBarLife = preload("LifeBarLife.tscn")

func _ready():
	fill()

func set_life_count(value):
	if value == life_count:
		return
	
	life_count = value

	fill()
	
func fill():

	var count = get_child_count()
	
	if life_count == count:
		return
	
	if life_count == null:
		life_count = 0
	
	if life_count > count:
		print("add", life_count - count)
		for i in range(life_count - count):
			add_child(LifeBarLife.instance())
	else:
		count -= life_count
		for child in get_children():
			child.queue_free()
			count -= 1
			if count == 0:
				break
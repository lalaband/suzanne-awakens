extends Control

signal next

var player

var level_text = ["So far so good. I know this Forest holds many other dangers. I have to stay cautious.", "This part of the Forest burnt down. I’m sure the Nefarious are behind all this...", "I’m going further into the Forest. The atmosphere here is so… unique.", "I can feel the danger increasing more and more. The Forest’s heart is probably not far now. I must stay vigilant.", "This is it. I reached the Forest’s heart. I feel ready."]
var advantage_text = {
	sword = "I bound to many swords in the past, but this one is just a piece of iron. Let’s do things differently.\nAttack altered",
	cape = "I feel better wearing this cape. But now is the time to focus on the real battle to come. I shall leave all tricks behind me.\nDodge altered",
	boots = "These boots proved themself useful while I was exploring the Forest. Maybe someone will find them.\nTravel speed altered",
	amulette = "This gem has been with me since I found it in a dungeon. I must learn to defend myself without its magic powers.\nRecovering altered",
	armor = "I came here shielded. I need to know what I’m worth with only my skin to protect me from the darkness.\nHealth altered",
	vision = "",
	ear = "",
	perception = "I suffered from many wounds when I was the protector of the Royal Family. Maybe I could try to forget the pain during battles?\nInterface altered",
	clairvoyance = ""
}

var level = -1

enum {LEVEL_FINISHED, SACRIFICE, MESSAGES}

func show(state):
	$VBoxContainer/FinishedLabel.visible = state == LEVEL_FINISHED
	
	$VBoxContainer/ChooseLabel.visible = state == SACRIFICE
	$VBoxContainer/HBoxContainer.visible = state == SACRIFICE
#	$VBoxContainer/SwordButton.visible = state == SACRIFICE
#	$VBoxContainer/CapeButton.visible = state == SACRIFICE
#	$VBoxContainer/BootsButton.visible = state == SACRIFICE
#	$VBoxContainer/AmuletteButton.visible = state == SACRIFICE
#	$VBoxContainer/ArmorButton.visible = state == SACRIFICE
	
	$VBoxContainer/MarginContainer.visible = state == MESSAGES
	$VBoxContainer/MarginContainer2.visible = state == MESSAGES
	$VBoxContainer/PlayButton.visible = state == MESSAGES


func start():
	show(LEVEL_FINISHED)
	$FinishedTimer.start()

func _on_FinishedTimer_timeout():
	show(SACRIFICE)
	
	$VBoxContainer/HBoxContainer/SwordButton.visible = player.has_sword
	$VBoxContainer/HBoxContainer/CapeButton.visible = player.has_cape
	$VBoxContainer/HBoxContainer/BootsButton.visible = player.has_boots
	$VBoxContainer/HBoxContainer/AmuletteButton.visible = player.has_amulette
	$VBoxContainer/HBoxContainer/ArmorButton.visible = player.has_armor
	$VBoxContainer/HBoxContainer/PerceptionButton.visible = player.has_perception

func _on_SwordButton_pressed():
	player.has_sword = false
	show_message("sword")

func _on_CapeButton_pressed():
	player.has_cape = false
	show_message("cape")


func _on_BootsButton_pressed():
	player.has_boots = false
	show_message("boots")


func _on_AmuletteButton_pressed():
	player.has_amulette = false
	show_message("amulette")

func _on_ArmorButton_pressed():
	player.has_armor = false
	show_message("armor")

func _on_PerceptionButton_pressed():
	player.has_perception = false
	show_message("perception")

func show_message(advantage):
	$VBoxContainer/MarginContainer/LevelLabel.text = level_text[level - 1]
	$VBoxContainer/MarginContainer2/AdvantageLabel.text = advantage_text[advantage]
	show(MESSAGES)

func _on_PlayButton_pressed():
	emit_signal("next")

func _on_NextWave_next():
	player.regen_life()


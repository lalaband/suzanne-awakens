extends Spatial

var spawn_count = 0
var spwan_finished = 0
var enemie_count = 0
var enemie_killed = 0

signal finished(result)

func _ready():
	$Player/Health.connect("killed", self, "_on_player_killed")
	
	for child in get_children():
		if child is preload("PathSpawn.gd"):
			spawn_count += 1
			enemie_count += child.number_of_enemies
			child.connect("spawn_finished", self, "_on_spawn_finished")
			child.connect("enemie_spawned", self, "_on_enemie_spawned")

func _on_spawn_finished():
	spwan_finished += 1
	check()

func _on_enemie_spawned(enemie):
	enemie.get_node("Health").connect("killed", self, "_on_enemie_killed")

func _on_enemie_killed():
	enemie_killed += 1
	check()

func check():
	if spwan_finished == spawn_count and enemie_killed == enemie_count:
		emit_signal("finished", "win")

func _on_player_killed():
	emit_signal("finished", "loose")

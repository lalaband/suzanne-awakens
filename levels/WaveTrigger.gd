extends Node

export(NodePath) var path_spawn_trigger
export(NodePath) var path_spawn_to_trigger
export(int) var enemies_need_be_killed_count = 0

var enemies_killed = 0

func _ready():
	var path_spawn = get_node(path_spawn_trigger)
	path_spawn.connect("enemie_spawned", self, "_on_enemie_spawned")

func _on_enemie_spawned(enemie):
	enemie.get_node("Health").connect("killed", self, "_on_enemie_killed", [], CONNECT_ONESHOT)

func _on_enemie_killed():
	enemies_killed += 1
	
	if enemies_killed == enemies_need_be_killed_count:
		var path_spawn = get_node(path_spawn_to_trigger)
		path_spawn.spawn()

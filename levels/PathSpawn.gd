extends Path

const ENEMIE_MALFAISANT = 1
const ENEMIE_BESTIAL = 2

export(NodePath) var player = "../Player"
export(int, FLAGS, "Malfaisant", "Bestial") var enemie = 0
export(float) var interval = 1 setget set_interval
export(int) var number_of_enemies = 0
export(bool) var enemie_idle = false
export(bool) var wait = false

var Malfaisant = preload("res://characters/enemies/malfaisant/Malfaisant.tscn")
var Bestial = preload("res://characters/enemies/bestial/Bestial.tscn")

var available_enemies = []
var enemie_count = 0

signal enemie_spawned(enemie)
signal spawn_finished()

func set_interval(value):
	interval = value
	
	if has_node("IntervalTimer"):
		$IntervalTimer.wait_time = value

func _ready():
	randomize()
	
	if enemie & ENEMIE_MALFAISANT:
		available_enemies.append(Malfaisant)
	if enemie & ENEMIE_BESTIAL:
		available_enemies.append(Bestial)
	
	$IntervalTimer.wait_time = interval
	
	if not wait:
		$IntervalTimer.start()

func spawn():
	if enemie_count >= number_of_enemies:
		return
	
	$SpawnPosition.unit_offset = randf()
	
	var enemie = available_enemies[randi() % available_enemies.size()].instance()
	enemie.player_node = get_node(player).get_path()
	enemie.transform = $SpawnPosition.transform
#	enemie.rotate_y(randf())
	
	if enemie_idle:
		enemie.state = enemie.IDLE
	
	add_child(enemie)
	
	enemie_count += 1
	$IntervalTimer.start()
	
	emit_signal("enemie_spawned", enemie)
	
	if enemie_count == number_of_enemies:
		emit_signal("spawn_finished")

func _on_IntervalTimer_timeout():
	spawn()

extends Area

export(NodePath) var player_node = "../Player"
var has_notified = false

onready var player = get_node(player_node)

func _on_WatchedEnemyArea_body_entered(body):
	if has_notified:
		return

	if body == player:
		has_notified = true
		for body in get_overlapping_bodies():
			if body is preload("res://characters/enemies/Enemie.gd"):
				body.change_state(body.RUN)

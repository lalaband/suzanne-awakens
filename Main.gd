extends Control

const LEVEL_COUNT = 6

var current_level
var level
var player
onready var nextwave = $NextWave

func _ready():
	$Background.visible = true
	$IntroScreen.visible = true

func start_game():
	$Background.visible = false
	$LoseScreen.visible = false
	$WonScreen.visible = false
	
	current_level = 0
	level = null
	player = preload("res://characters/player/Player.tscn").instance()
	
	next_level()

func _on_level_finished(result):
	if result == "loose" or current_level == LEVEL_COUNT:
		game_finished(result)
	else:
		sacrifice()

func sacrifice():
	remove_child(nextwave)
	level.add_child(nextwave)
	nextwave.visible = true
	nextwave.player = player
	nextwave.level = current_level
	nextwave.start()

func game_finished(result):
	level.remove_child(player)
	level.queue_free()
	add_child(player)
	player.queue_free()
	
	$Background.visible = true
	match result:
		"win":
			$WonScreen.visible = true
		"loose":
			$LoseScreen.visible = true

func next_level():
	
	if level != null:
		level.remove_child(player)
		level.queue_free()
	
	current_level += 1
	
	level = load("res://levels/Level" + String(current_level) + ".tscn").instance()
	level.add_child(player)
	
	player.transform = level.get_node("PlayerSpawn").transform
	
	level.connect("finished", self, "_on_level_finished")
	
	add_child(level)

func _on_ContinueButton_pressed():
	$IntroScreen.visible = false
	$TutoScreen.visible = true

func _on_PlayButton_pressed():
	$TutoScreen.visible = false
	start_game()

func _on_RetryButton_pressed():
	start_game()

func _on_PlayAgainButton_pressed():
	start_game()

func _on_NextWave_next():
	nextwave.visible = false
	level.remove_child(nextwave)
	add_child(nextwave)
	next_level()

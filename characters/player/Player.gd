extends KinematicBody

var MOVE_SPEED = 10
var ACCEL = 4.5
var DEACCEL = 16

var DASH_SPEED = 200
var DASH_DISTANCE = 500

var SWORD_DAMAGE = 2
var DASH_DAMAGE = 1

var dir = Vector3()
var vel = Vector3()
var distance = 0.0
var dash_available = true

var input_movement = Vector2()
var orientation = Vector2(0, 1)

enum State {IDLE, MOVE, DASH, ATTACK}
var state = null

enum Anim {IDLE_RUN, DASH, ATTACK_SOWRD, HIGH_KICK}

onready var AnimationTreePlayer = $Spatial/AnimationTreePlayer

var has_sword = true setget set_has_sword
var has_cape = true setget set_has_cape
var has_boots = true setget set_has_boots
var has_armor = true setget set_has_armor
var has_amulette = true setget set_has_amulette
var has_perception = true setget set_has_perception

func set_has_sword(value):
	if has_sword == value:
		return
	
	has_sword = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/sword.visible = value
	
	if has_sword:
		SWORD_DAMAGE += 1
	else:
		SWORD_DAMAGE -= 1

func set_has_cape(value):
	if has_cape == value:
		return
	
	has_cape = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/cape.visible = value

func set_has_boots(value):
	if has_boots == value:
		return
	
	has_boots = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/boots.visible = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/socks.visible = not value
	
	if has_boots:
		MOVE_SPEED /= (1 - 0.3)
	else:
		MOVE_SPEED *= (1 - 0.3)

func set_has_armor(value):
	if has_armor == value:
		return
	
	has_armor = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/armor.visible = value
	
	if has_armor:
		$Health.amount_of_life += 1
	else:
		$Health.amount_of_life -= 1

func set_has_amulette(value):
	if has_amulette == value:
		return
	
	has_amulette = value
	$Spatial/Suzanne/suzanne_Armature/Skeleton/amulette.visible = value

func set_has_perception(value):
	if has_perception == value:
		return
	
	has_perception = value
	$Hud/LifeBar.visible = value

func _ready():
	AnimationTreePlayer.active = true
	$Hud/LifeBar.life_count = $Health.life
	change_state(State.IDLE)

func _physics_process(delta):
	process_input(delta)
	process_movement(delta)

func process_input(delta):
	match state:
		State.IDLE:
			idle_process_input(delta)
		State.MOVE:
			move_process_input(delta)

func process_movement(delta):
	match state:
		State.MOVE:
			move_process_mouvement(delta)
		State.DASH:
			dash_process_mouvement(delta)

func change_state(new_state):
	match state:
		State.DASH:
			vel = Vector3()
			$DashTimer.start()
		State.ATTACK:
			var bodies
			
			if has_sword:
				bodies = $Spatial/SwordAttackArea.get_overlapping_bodies()
			else:
				bodies = $Spatial/HighKickAttackArea.get_overlapping_bodies()
			
			for body in bodies:
				if not body.has_node("Health"):
					continue

				var health = body.get_node("Health")
				health.take_damage(SWORD_DAMAGE)
	
#	if new_state != state:
#		print(new_state)
	
	state = new_state
	
	match new_state:
		State.IDLE:
			AnimationTreePlayer.blend2_node_set_amount("idle-run", 0.0)
			AnimationTreePlayer.transition_node_set_current("state", Anim.IDLE_RUN)
		State.MOVE:
			AnimationTreePlayer.transition_node_set_current("state", Anim.IDLE_RUN)
		State.DASH:
			AnimationTreePlayer.transition_node_set_current("state", Anim.DASH)
			dash_available = false
			distance = 0.0
			
			vel = Vector3()
			
			dir = Vector3()
			dir += - get_global_transform().basis.z.normalized() * orientation.y
			dir += get_global_transform().basis.x.normalized() * orientation.x
		State.ATTACK:
			if has_sword:
				AnimationTreePlayer.transition_node_set_current("state", Anim.ATTACK_SOWRD)
			else:
				AnimationTreePlayer.transition_node_set_current("state", Anim.HIGH_KICK)
			
			$AttackTimer.start()


func _on_DashTimer_timeout():
	dash_available = true


func idle_process_input(delta):
	update_input_mouvement()
	
	if Input.is_action_pressed("attack"):
		change_state(State.ATTACK)
		return
	
	if Input.is_action_pressed("dash") and dash_available and has_cape:
		change_state(State.DASH)
		return
	
	if input_movement != Vector2():
		change_state(State.MOVE)
		return

func move_process_input(delta):
	update_input_mouvement()
	
	if Input.is_action_pressed("attack"):
		change_state(State.ATTACK)
		return
	
	if Input.is_action_pressed("dash") and dash_available and has_cape:
		change_state(State.DASH)
		return

func update_input_mouvement():
	input_movement = Vector2()
	
	if Input.is_action_pressed("movement_forward"):
		input_movement.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement.x += 1
	
	input_movement = input_movement.normalized()
	
	if input_movement != Vector2():
		orientation = input_movement
		
		var arrow = $Spatial
		arrow.transform.basis = Basis()
		arrow.rotate_y(Vector2(0, 1).angle_to(orientation))
	
	dir = Vector3()
	dir += - get_global_transform().basis.z.normalized() * input_movement.y
	dir += get_global_transform().basis.x.normalized() * input_movement.x

func move_process_mouvement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir
	target *= MOVE_SPEED
	
#	var dot = abs(Vector2(0, 1).dot(Vector2(dir.x, dir.z).normalized()))
#	target.z *= 2.0 * dot
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

#	print("idle-run : ", min(hvel.length() / MOVE_SPEED, 1.0))
	AnimationTreePlayer.blend2_node_set_amount("idle-run", min(hvel.length() / MOVE_SPEED, 1.0))
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0))
	vel.y = 0
	translation.y = 0.0
	
	if round(vel.length()) == 0.0:
		change_state(State.IDLE)

func dash_process_mouvement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir
	target *= DASH_SPEED
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	var collision = move_and_collide(vel * delta)
	
	if collision != null:
		if collision.collider.has_node("Health"):
			var health = collision.collider.get_node("Health")
			health.take_damage(DASH_DAMAGE)
			if not health.life == 0:
				change_state(State.IDLE)
				return
		else:
			change_state(State.IDLE)
			return
	
	distance += vel.length()
	if distance >= DASH_DISTANCE:
		change_state(State.IDLE)


func _on_AttackTimer_timeout():
	change_state(State.IDLE)

func regen_life():
	if has_amulette:
		$Health.heal($Health.amount_of_life)
	else:
		$Health.heal(round($Health.amount_of_life * 0.5))
	

func _on_Health_life_changed(life):
	$Hud/LifeBar.life_count = life

extends KinematicBody

export(NodePath) var player_node = "../Player"
export(float) var MAX_SPEED = 5
export(int) var DAMAGE = 1

onready var player = get_node(player_node)
onready var animation = $Pivot/AnimationTreePlayer

enum Anim {IDLE_RUN, ATTACK}
enum State {RUN, ATTACK, IDLE}
var state = RUN

var attack_available = true
var player_reachable = false

func _ready():
	animation.active = true
	change_state(state)

func _physics_process(delta):
	if state != RUN:
		return

	var dir = player.transform.origin - transform.origin
	dir = dir.normalized()
	move_and_slide(dir * MAX_SPEED)
	translation.y = 0.0
	
	$Pivot.transform.basis = Basis()
	$Pivot.rotate_y(Vector2(0, 1).angle_to(Vector2(-dir.x, dir.z)))

func _on_Health_killed():
	queue_free()

func _on_AttackTimer_timeout():
	change_state(RUN)
	if $Pivot/AttackArea.get_overlapping_bodies().has(player):
		player.get_node("Health").take_damage(DAMAGE)
	
	$DelayAttackTimer.start()

func _on_DelayAttackTimer_timeout():
	attack_available = true
	
	attack()


func _on_AttackArea_body_entered(body):
	if body == player:
		player_reachable = true
		
	attack()


func _on_AttackArea_body_exited(body):
	if body == player:
		player_reachable = false

func attack():
	if not attack_available or not player_reachable:
		return
	
	change_state(ATTACK)
	attack_available = false
	$AttackTimer.start()

func change_state(new_state):
	
	match new_state:
		ATTACK:
			animation.transition_node_set_current("state", Anim.ATTACK)
		RUN:
			animation.blend2_node_set_amount("idle-run", 1.0)
			animation.transition_node_set_current("state", Anim.IDLE_RUN)
		IDLE:
			animation.blend2_node_set_amount("idle-run", 0.0)
			animation.transition_node_set_current("state", Anim.IDLE_RUN)
	
	state = new_state

extends Node

export(int) var amount_of_life = 0

var life

signal life_changed(life)
signal killed

func _ready():
	life = amount_of_life

func take_damage(damage):
	life = max(life - damage, 0)
	print("new life : ", life)
	
	emit_signal("life_changed", life)
	
	if life == 0:
		emit_signal("killed")

func heal(amount):
	life = min(life + amount, amount_of_life)
	
	emit_signal("life_changed", life)

func full_heal():
	life = amount_of_life
	
	emit_signal("life_changed", life)